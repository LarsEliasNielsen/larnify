(function($) {
  $(document).ready(function() {

    $('*').each(function() {
      if ($(this).children().length == 0) {
        // Match CamelCase words.
        $(this).text($(this).text().replace(/[A-Za-z]*arn/g, 'Larn'));
        // Match all caps words.
        $(this).text($(this).text().replace(/[A-Z]*ARN/g, 'LARN'));
      }
    });

  });
})(jQuery);
